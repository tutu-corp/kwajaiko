//
//  SymbolStyle.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import SwiftUI

extension Image {
    func SymbolStyle() -> some View {
        self
            .resizable()
            .scaledToFit()
            .frame(width: 12, height: 12)
            .padding(6)
            .overlay(
                Circle()
                    .stroke(Color.primary,
                            lineWidth:1)
            )
    }
}

struct SymbolStyle_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            Image(systemName: "speaker.wave.2")
                .SymbolStyle()
        }
    }
}


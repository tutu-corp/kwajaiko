//
//  CardStyle.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 27/08/2023.
//
//  

import SwiftUI

extension View {
    func cardStyle(color: Color) -> some View {
        self
            .padding()
            .background(Color(UIColor.secondarySystemBackground))
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .fill(color)
                    .opacity(0.6)
                    .mask(HStack {
                        Rectangle().frame(width: 10)
                        Spacer()
                    })
            )
    }
}

struct CardStyle_Previews: PreviewProvider {
    static var previews: some View {
        Text("Preview")
            .cardStyle(color: .green)
    }
}

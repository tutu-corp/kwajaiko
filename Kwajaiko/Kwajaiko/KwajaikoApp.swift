//
//  KwajaikoApp.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import SwiftUI

@main
struct KwajaikoApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}

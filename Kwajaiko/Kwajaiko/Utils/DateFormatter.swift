//
//  DateFormatter.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 28/08/2023.
//
//  

import Foundation

extension Date {
    func toDateTimeString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "d/MM/yyyy"
        return formatter.string(from: self)
    }
}



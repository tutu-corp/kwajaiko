//
//  StringExt.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 28/08/2023.
//
//  

import Foundation

extension String {
    func isNotEmpty() -> Bool{
      return !self.isEmpty
    }

    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }

    func toDateTime() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: self)!
    }
}

//
//  Repository.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 02/09/2023.
//
//  

import Foundation

class Repository {
    
//    static var shared : MainRepository = FakeMainRepository()
    static var shared : MainRepository = DefaultMainRepository()
    
    private init(){
        
    }

}

protocol MainRepository {
    
    var list: [Order] {get set}
    
    func addNewOrder(_ order: Order)
    
    func removeOrder(index: Int)
}

class DefaultMainRepository: MainRepository {
    
    var list: [Order] = UserDefaults.standard.orders

    func addNewOrder(_ order: Order) {
        list.append(order)
        UserDefaults.standard.orders = list
    }
    
    func removeOrder(index: Int) {
        list.remove(at: index)
        UserDefaults.standard.orders = list
    }
}

class FakeMainRepository: MainRepository {
    var list: [Order] = [
        Order(
            name: "Pull de Noël",
            favorite: false,
            expectedPreorder: "10/12/2023".toDateTime(),
            preorderDateState: .isDated),
        Order(
            name: "La belle et la bête",
            favorite: false,
            preorderDateState: .isUndated,
            link: URL(string: "https://cdn.discordapp.com/attachments/1140352169418166402/1146016289027002378/IMG_4844.png")!
           )
    ]
    
    func addNewOrder(_ order: Order) {
        list.append(order)
    }
    
    func removeOrder(index: Int) {
        list.remove(at: index)
    }
    
}

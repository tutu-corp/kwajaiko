//
//  Order.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import Foundation

struct Order: Equatable, Codable {
    var id: UUID = UUID()
    
//    Name (String) + Favorite (Bool)
    var name: String
    var favorite: Bool

    
//    releaseDate(Date?)
//    preOrder(Bool)
    
    var expectedPreorder : Date?
    var preorderDateState: PreorderDateState
    var link: URL?
}


extension UserDefaults {
    var orders: [Order] {
        get {
            guard let data = UserDefaults.standard.data(forKey: "orders") else { return [] }
            return (try? PropertyListDecoder().decode([Order].self, from: data)) ?? []
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: "orders")
        }
    }
}

//
//  PreorderDateState.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import Foundation
import SwiftUI

enum PreorderDateState : Codable {
    case isDated, isUndated
    
    func color() -> Color {
        switch self {
        case .isDated:
            return Color("bluePastel")
        case .isUndated:
            return Color("pinkPastel")
        }
    }
}






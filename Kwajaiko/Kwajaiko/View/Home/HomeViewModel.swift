//
//  HomeViewModel.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import SwiftUI

class HomeViewModel: ObservableObject {
    
    @Published var showUndated = true
    
    @Published var list = Repository.shared.list
    
    var filteredOrders: [Order] {
        if showUndated == true {
            return list
        } else {
            return list.filter { order in
                order.preorderDateState == .isDated
            }
        }
    }
    func refresh() {
        list = Repository.shared.list
    }
    func removeOrder(index: Int) {
        Repository.shared.removeOrder(index: index)
        refresh()
    }
}

//
//  HomeView.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var viewModel: HomeViewModel = HomeViewModel()
    @State private var isFormSheetOpen = false
    
    var body: some View {
        VStack{
            HStack {
                Text("Ce mois ci")
                    .font(.title)
                Spacer()
            }
            .padding(.horizontal)
            
            cardList
            Divider()
            HStack{
                filterButton
                Spacer()
                addOrderButton
            }
            .padding(.horizontal)
        }
        .onAppear(perform: viewModel.refresh)
        .padding(2)
        .sheet(isPresented: $isFormSheetOpen) {
            NewOrderView(){
                isFormSheetOpen = false
                viewModel.refresh()
            }
        }
    }
    
    private var cardList : some View {
        ScrollView {
            Spacer()
                .frame(height: 2)
            LazyVStack {
                ForEach(viewModel.filteredOrders, id: \.id) { order in
                    CardView(order: order)
                        .contentShape(.contextMenuPreview, RoundedRectangle(cornerRadius: 10))
                        .contextMenu {
                            cardContextMenu(order: order)
                        }
                }
            }
            .padding(.horizontal)
        }
    }
    
    private func cardContextMenu(order: Order) -> some View {
        Group {
            if let link = order.link {
                Link(destination: link) {
                    Text("Voir la commande")
                    Image(systemName: "link")
                    
                }
            }
            Button {
                print("Editer la card")
            } label: {
                Label("Editer", systemImage: "pencil")
            }
            
            Button {
                print("Ajouter aux favoris")
            } label: {
                Label("Ajouter aux favoris", systemImage: "star")
            }
            
            Button {
                print("Supprimer")
                viewModel.removeOrder(index: viewModel.list.firstIndex(of: order)!)
            } label: {
                Label("Supprimer", systemImage: "minus.circle")
            }
        }
    }
    
    private var filterButton : some View {
        Button("Masquer les non datés") {
            viewModel.showUndated.toggle()
        }
        .buttonStyle(.borderedProminent)
        .tint(viewModel.showUndated ? Color(UIColor.systemGray3) : Color("bluePastel"))
        .padding(.horizontal)
    }
    
    private var addOrderButton : some View {
        Button() {
            isFormSheetOpen.toggle()
        } label: {
            
            Image(systemName: "plus.circle.fill")
        }
        .buttonStyle(.borderedProminent)
        .tint(Color("tealPastel"))
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

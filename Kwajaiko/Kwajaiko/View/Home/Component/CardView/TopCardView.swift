//
//  TopCardView.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 27/08/2023.
//
//  

import SwiftUI

struct TopCardView: View {
    
    var title : String
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                HStack {
                    Text(title)
                        .font(.title3)
                    Spacer()
                }
            }
        }
    }
}

struct TopCardView_Previews: PreviewProvider {
    static var previews: some View {
        TopCardView(title: "Le Produit")
    }
}



//
//  FooterCardView.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 27/08/2023.
//
//  

import SwiftUI

struct FooterCardView: View {
    
    var expectedDate : Date?
    
    var body: some View {
        
        if let expectedDate {
            preorderFooter(expectedDate: expectedDate)
        } else {
            orderFooter
        }
    }
    
    private var orderFooter : some View {
        HStack {
            Image(systemName: "moon.stars.fill")
                .SymbolStyle()
            Text("Pas de date de sortie")
                .font(.footnote)
            Spacer()
        }
    }
    
    private func preorderFooter(expectedDate: Date) -> some View {
        HStack {
            Image(systemName: "hourglass")
                .SymbolStyle()
            VStack (alignment: .leading) {
                Text("Précommandé pour le \(expectedDate.toDateTimeString())")
                    .font(.footnote)
            }
            Spacer()
        }
    }
}

struct FooterCardView_Previews: PreviewProvider {
    static var previews: some View {
        FooterCardView(expectedDate: "12/09/2023".toDateTime())
    }
}

//
//  CardView.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 26/08/2023.
//
//  

import SwiftUI

struct CardView: View {
    
    var preorderDateState : PreorderDateState
    
    var name : String
    
    var expectedDate : Date?
    
    init(order:Order){
        self.preorderDateState = order.preorderDateState
        self.name = order.name
        self.expectedDate = order.expectedPreorder
    }

    var body: some View {
        VStack {
            TopCardView(
                title: name
            )
            
            Divider()
            
            FooterCardView(
                expectedDate: expectedDate
            )
        }
        .cardStyle(color: preorderDateState.color())
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        let orderPreview = Order(name: "Le Produit", favorite: false, expectedPreorder: "29/10/2023".toDateTime(), preorderDateState: .isDated)
        
        CardView(order: orderPreview)
    }
}

//
//  NewOrderViewModel.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 28/08/2023.
//
//  

import Foundation

class NewOrderViewModel : ObservableObject {
    
    // Order
    @Published var name : String = ""
    @Published var shop : String = ""
    @Published var startingDate = Date()
    @Published var link : String = ""
    @Published var expectedOrder: Date?
    @Published var isUndated = false
    
    func sendForm() throws {
        let order = try createOrder()
        print(order)
        
        Repository.shared.addNewOrder(order)
    }
    
    func createOrder() throws -> Order {
        try validateForm()
        
        var preorderDateState = definePreorderDateState(state: isUndated)
        
        var linkToURL: URL? = nil
        
        if link != ""  {
            
            if link.isValidURL{
                linkToURL = URL(string: link)
            } else if link.isEmpty {
                linkToURL = nil
            } else {
                linkToURL = nil
                throw "Votre lien est incorrect"
            }
        }
        
        let order = Order(
            name: name,
            favorite: false,
            expectedPreorder: expectedOrder,
            preorderDateState: preorderDateState,
            link: linkToURL
        )
        return order
    }
    
    func validateForm() throws {
        if name.isEmpty || shop.isEmpty {
            throw "Merci de completer tous les champs"
        }
    }
    
    func definePreorderDateState(state: Bool) -> PreorderDateState {
        if state {
            return .isUndated
        } else {
            return .isDated
        }
    }
}

extension String: Error {}

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}

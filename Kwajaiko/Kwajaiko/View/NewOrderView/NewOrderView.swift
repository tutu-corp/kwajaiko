//
//  NewOrderView.swift of project named Kwajaiko
//
//  Created by Aurore D (@Tuwleep) on 28/08/2023.
//
//  

import SwiftUI

struct NewOrderView: View {
    
    @ObservedObject var viewModel : NewOrderViewModel = NewOrderViewModel()
    @State var errorDialog : String? = nil
    @State var errorDialogShowed : Bool = false
    var closeSheet : () -> ()
    
    var body: some View {
        List {
            Section(header:HStack {
                Text("Information")
            }
            ){
                
                TextField("Nom de la commande", text: $viewModel.name)
                TextField("Lien de l'achat", text: $viewModel.link)
                
                Toggle("Date inconnue", isOn: $viewModel.isUndated)
                    .onChange(of: viewModel.isUndated) { value in
                        if value {
                            print("Pas de date")
                            viewModel.expectedOrder = nil
                        } else {
                            print("date")
                            viewModel.expectedOrder = Date()
                        }
                    }
                if viewModel.isUndated == false {
                    DatePicker("Date de sortie ",
                               selection: Binding<Date>(
                                get: {self.viewModel.expectedOrder ?? Date()},
                                set: {self.viewModel.expectedOrder = $0
                                }),
                               displayedComponents: .date)
                }
            }
        }
        
        Button("Valider") {
            
            do {
                try viewModel.sendForm()
                closeSheet()
            } catch {
                errorDialog = error.localizedDescription
                errorDialogShowed = true
            }
        }
        .alert(errorDialog ?? "", isPresented: $errorDialogShowed) {
            Button("OK", role: .cancel) {
                errorDialogShowed = false
            }
        }
        
        
    }
}


struct NewOrderView_Previews: PreviewProvider {
    static var previews: some View {
        NewOrderView(){
            
        }
    }
}
